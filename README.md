# README #

Database project for the [Collaborative Health Exchange](https://bitbucket.org/rjensen11/collaborativehealthexchange)

### How do I get set up? ###

* The schema is designed for Microsoft Database Server and can be depoloyed throught the Managment Studio or using a Visual Studio Database project
* (Alternately) The web project uses Entity Framework and could be configured to run code first

### Use guidelines ###

* The project is a sample of my work and can be used or modified freely as long as the [license](https://bitbucket.org/rjensen11/collaborativehealth-db/raw/c0d9e1a256e5781a6deeb88a3d43b332cb0815dc/LICENSE.txt) and attribution is maintained in the project

### Who do I talk to? ###

* Ryan Jensen ([rjensen.professional@gmail.com](mailto:rjensen.professional@gmail.com?Subject=CollaborativeHealthExchange))