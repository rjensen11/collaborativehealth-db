﻿CREATE TABLE [dbo].[Role] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (75) NOT NULL,
    [DisplayName]      NVARCHAR (75) NOT NULL,
    [IsSelfAssignable] BIT           CONSTRAINT [DF_Role_IsSelfAssignable] DEFAULT ((0)) NOT NULL,
    [CreationDate]     DATETIME      NOT NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([ID] ASC)
);

