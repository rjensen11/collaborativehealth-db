﻿CREATE TABLE [dbo].[ForumPost] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [Body]         NVARCHAR (MAX) NOT NULL,
    [Title]        NVARCHAR (300) NOT NULL,
    [UserID]       INT            NOT NULL,
    [Views]        INT            CONSTRAINT [DF_ForumPost_Views] DEFAULT ((0)) NOT NULL,
    [Edited]       BIT            CONSTRAINT [DF_ForumPost_Edited] DEFAULT ((0)) NOT NULL,
    [CreationDate] DATETIME       NULL,
    [EditDate]     DATETIME       NULL,
    [Removed]      BIT            CONSTRAINT [DF_ForumPost_Deleted] DEFAULT ((0)) NOT NULL,
    [RemovedDate]  DATETIME       NULL,
    CONSTRAINT [PK_ForumPost] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ForumPost_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);





