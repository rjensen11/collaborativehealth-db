﻿CREATE TABLE [dbo].[MessageHeader] (
    [ID]                INT            IDENTITY (1, 1) NOT NULL,
    [Subject]           NVARCHAR (200) NULL,
    [SendingUserID]     INT            NOT NULL,
    [ReceivingUserID]   INT            NOT NULL,
    [CreationDate]      DATETIME       NOT NULL,
    [SendingUserRead]   BIT            CONSTRAINT [DF_MessageHeader_SendingUserRead] DEFAULT ((0)) NOT NULL,
    [ReceivingUserRead] BIT            CONSTRAINT [DF_MessageHeader_ReceivingUserRead] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MessageHeader] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_MessageHeader_User_Receiver] FOREIGN KEY ([ReceivingUserID]) REFERENCES [dbo].[User] ([ID]),
    CONSTRAINT [FK_MessageHeader_User_Sender] FOREIGN KEY ([SendingUserID]) REFERENCES [dbo].[User] ([ID])
);

