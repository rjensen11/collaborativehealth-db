﻿CREATE TABLE [dbo].[ForumPostVote] (
    [ID]           INT      IDENTITY (1, 1) NOT NULL,
    [IsUpvote]     BIT      CONSTRAINT [DF_ForumPostVote_IsUpvote] DEFAULT ((0)) NOT NULL,
    [UserID]       INT      NOT NULL,
    [ForumPostID]  INT      NOT NULL,
    [CreationDate] DATETIME NOT NULL,
    CONSTRAINT [PK_ForumPostVote] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_ForumPostVote_ForumPost] FOREIGN KEY ([ForumPostID]) REFERENCES [dbo].[ForumPost] ([ID]),
    CONSTRAINT [FK_ForumPostVote_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);

