﻿CREATE TABLE [dbo].[CommentVote] (
    [ID]           INT      IDENTITY (1, 1) NOT NULL,
    [IsUpvote]     BIT      CONSTRAINT [DF_CommentVote_IsUpvote] DEFAULT ((0)) NOT NULL,
    [UserID]       INT      NOT NULL,
    [CommentID]    INT      NOT NULL,
    [CreationDate] DATETIME NOT NULL,
    CONSTRAINT [PK_CommentVote] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_CommentVote_Comment] FOREIGN KEY ([CommentID]) REFERENCES [dbo].[Comment] ([ID]),
    CONSTRAINT [FK_CommentVote_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);

