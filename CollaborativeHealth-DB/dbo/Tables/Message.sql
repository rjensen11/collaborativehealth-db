﻿CREATE TABLE [dbo].[Message] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [Body]                NVARCHAR (MAX) NOT NULL,
    [SendingUserID]       INT            NOT NULL,
    [ReceivingUserID]     INT            NOT NULL,
    [MessageHeaderID]     INT            NOT NULL,
    [CreationDate]        DATETIME       NOT NULL,
    [SendingUserDelete]   BIT            CONSTRAINT [DF_Message_SendingUserDelete] DEFAULT ((0)) NOT NULL,
    [ReceivingUserDelete] BIT            CONSTRAINT [DF_Message_ReceivingUserDelete] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Message_MessageHeader] FOREIGN KEY ([MessageHeaderID]) REFERENCES [dbo].[MessageHeader] ([ID]),
    CONSTRAINT [FK_Message_User_Receiver] FOREIGN KEY ([ReceivingUserID]) REFERENCES [dbo].[User] ([ID]),
    CONSTRAINT [FK_Message_User_Sender] FOREIGN KEY ([SendingUserID]) REFERENCES [dbo].[User] ([ID])
);







