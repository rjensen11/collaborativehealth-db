﻿CREATE TABLE [dbo].[Comment] (
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    [Body]         NVARCHAR (MAX) NOT NULL,
    [UserID]       INT            NOT NULL,
    [ForumPostID]  INT            NOT NULL,
    [Edited]       BIT            CONSTRAINT [DF_Comment_Edited] DEFAULT ((0)) NOT NULL,
    [CreationDate] DATETIME       NOT NULL,
    [EditDate]     DATETIME       NULL,
    [Removed]      BIT            CONSTRAINT [DF_Comment_Deleted] DEFAULT ((0)) NOT NULL,
    [RemovedDate]  DATETIME       NULL,
    CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Comment_ForumPost] FOREIGN KEY ([ForumPostID]) REFERENCES [dbo].[ForumPost] ([ID]),
    CONSTRAINT [FK_Comment_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID])
);



