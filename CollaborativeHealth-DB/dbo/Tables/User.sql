﻿CREATE TABLE [dbo].[User] (
    [ID]                         INT            IDENTITY (1, 1) NOT NULL,
    [Email]                      NVARCHAR (300) NOT NULL,
    [PassHash]                   NVARCHAR (75)  NULL,
    [FirstName]                  NVARCHAR (75)  NOT NULL,
    [LastName]                   NVARCHAR (75)  NOT NULL,
    [Active]                     BIT            CONSTRAINT [DF_User_Active] DEFAULT ((0)) NOT NULL,
    [ShouldResetPasswordOnLogin] BIT            CONSTRAINT [DF_User_ShouldResetPasswordOnLogin] DEFAULT ((0)) NOT NULL,
    [AccessToken]                NVARCHAR (75)  NULL,
    [TokenValidUntil]            DATETIME       NULL,
    [HasVerifyedEmail]           BIT            CONSTRAINT [DF_User_HasVerifyedEmail] DEFAULT ((0)) NOT NULL,
    [DateJoined]                 DATETIME       NULL,
    [LastLogin]                  DATETIME       NULL,
    [Removed]                    BIT            CONSTRAINT [DF_User_Deleted] DEFAULT ((0)) NOT NULL,
    [RemovedDate]                DATETIME       NULL,
    [IsSystemAdmin]              BIT            CONSTRAINT [DF_User_IsSystemAdmin] DEFAULT ((0)) NOT NULL,
    [ProfilePicture]             VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([ID] ASC)
);









